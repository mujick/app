import 'package:flutter_test/flutter_test.dart';
import 'package:mujick/player/heatmapcomputer.dart';

void main() {
  test("test", () {
    final density = 4;
    final List<int> raw = [1, 1, 1, 2, 2, 4, 8, 4, 20, 21, 20, 13, 56, 56, 54];
    HeatMapComputer computer = HeatMapComputer();
    HeatMapNormalizer normalizer = HeatMapNormalizer();
    List<Cluster> clusters = computer.compute(raw, density, density ~/ 2);

    print(clusters);

    clusters = normalizer.normalize(clusters, 60, 300, 100, 0.3);

    print(clusters);
  });
}
