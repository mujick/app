import 'package:flutter/material.dart';
import 'package:mujick/entity/Track.dart';
import 'package:mujick/player/heatmap.dart';
import 'package:mujick/player/playerpage.dart';
import 'package:mujick/ui/tracks/TracksListWidget.dart';

class Router {
  static Route generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case root:
        return _buildRoute((context) => TracksList());
      case player:
        return _buildRoute(
            (context) => PlayerPage(settings.arguments as Track));
      case test:
        return _buildRoute((context) => TestHeatMapPage());
      default:
        throw Exception("Unkonwn route has been called: ${settings.name}");
    }
  }

  static const String root = "/";
  static const String player = "/player";
  static const String test = "/test";
}

Route _buildRoute(WidgetBuilder builder) {
  return MaterialPageRoute(builder: builder);
}
