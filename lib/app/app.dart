import 'package:flutter/material.dart';
import 'package:mujick/ResColors.dart';
import 'package:mujick/app/routing.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        onGenerateRoute: Router.generateRoute,
        theme: ThemeData.dark().copyWith(accentColor: ResColors.accent));
  }
}
