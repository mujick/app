import 'package:flutter/services.dart';

String splitLast(String string, String splitter) {
  int index = string.lastIndexOf(splitter);
  return string.substring(index + 1);
}

Future<bool> isAssetExist(String path) async {
  try {
    await rootBundle.load(path);
    return true;
  } catch (_) {
    return false;
  }
}
