class ClickDebouncer {
  static int _timeout = 5000;
  static int lastClick = 0;

  static void click() {
    lastClick = DateTime.now().millisecondsSinceEpoch;
  }

  static bool isClickAllowed() {
    final int current = DateTime.now().millisecondsSinceEpoch;
    return (current - lastClick) > _timeout;
  }
}
