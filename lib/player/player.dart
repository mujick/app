import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/foundation.dart';
import 'package:mujick/app/utils.dart' as utils;
import 'package:mujick/entity/Track.dart';

abstract class Prepareble<T> {
  Future<T> prepare(T withValue);
}

abstract class Stoppable {
  Future stop();
}

abstract class Player implements Stoppable, Prepareble<Track> {
  Future play();

  Future pause();
}

abstract class BasePlayer<T> implements Player {
  final T _player;
  Track _current;

  Track get current => _current;

  T get player => _player;

  bool get isPrepared => _current != null;

  BasePlayer(this._player);

  @mustCallSuper
  @override
  Future<Track> prepare(Track track) async {
    _current = track;
    return _current;
  }

  @mustCallSuper
  @override
  Future stop() async {
    _current = null;
  }
}

class AssetsPlayer extends BasePlayer<AssetsAudioPlayer> {
  AssetsPlayer(AssetsAudioPlayer player) : super(player);

  @override
  Future<Track> prepare(Track track) async {
    await super.prepare(track);

    final String assetPath = utils.splitLast(current.mp3Url, "/");
    final String folderPath = "assets/audios/";
    final String path = folderPath + assetPath;
    final bool assetExists = await utils.isAssetExist(path);
    if (!assetExists) {
      print("Asset $path does not exist");
      return null;
    }
    print("Loading asset $path");
    AssetsAudio asset = AssetsAudio(
      asset: assetPath,
      folder: folderPath,
    );
    _player.open(asset);
    PlayingAudio audio = await _player.current.first;
    _current = Track(_current.id, _current.name, _current.mp3Url,
        audio.duration.inMilliseconds, _current.heatMap);
    print("$Track loaded: $_current");
    await pause();
    return _current;
  }

  @override
  Future play() async {
    _player.play();
  }

  @override
  Future pause() async {
    _player.pause();
  }

  @override
  Future stop() async {
    await super.stop();
    _player.stop();
  }
}

typedef OnProgressUpdateCallback = Function(int ts);

typedef OnStatusUpdateCallback = Function(PlayerStatus status);

enum PlayerState { init, paused, playing }

class PlayerStatus {
  static const PlayerStatus init = const PlayerStatus(PlayerState.init);

  final bool started;
  final PlayerState state;
  final Track track;

  const PlayerStatus(this.state, {this.track, this.started = false});

  const PlayerStatus.paused(Track track, {bool started = false})
      : this(PlayerState.paused, track: track, started: started);

  PlayerStatus._playing(PlayerStatus status)
      : this(PlayerState.playing, track: status.track, started: true);

  bool get isPaused => state == PlayerState.paused;

  PlayerStatus toPlaying() {
    return PlayerStatus._playing(this);
  }

  PlayerStatus toPaused() {
    return PlayerStatus.paused(track, started: this.started);
  }

  @override
  String toString() {
    return "$state, track: $track";
  }
}

abstract class PlayerPanel implements Stoppable, Prepareble<Track> {
  PlayerStatus get status;

  Future playPause();

  Future seek(Duration duration);

  Stream<Duration> get progress;

  void onStatusUpdated(OnStatusUpdateCallback callback);
}

class BasePlayerPanel<T> implements PlayerPanel {
  PlayerStatus _status = PlayerStatus.init;
  BasePlayer<T> basePlayer;
  OnStatusUpdateCallback statusCallback;
  OnProgressUpdateCallback progressCallback;

  BasePlayerPanel(this.basePlayer);

  @override
  PlayerStatus get status => _status;

  @override
  Stream<Duration> get progress => Stream.value(Duration(seconds: 0));

  @override
  Future<Track> prepare(Track track) async {
    Track tmp = await basePlayer.prepare(track);
    _updateStatus(PlayerStatus.paused(tmp));
    return basePlayer.current;
  }

  @override
  Future stop() {
    _updateStatus(PlayerStatus.init, silent: true);
    return basePlayer.stop();
  }

  @override
  Future playPause() {
    if (_status.isPaused) {
      return basePlayer
          .play()
          .whenComplete(() => _updateStatus(_status.toPlaying()));
    } else {
      return basePlayer
          .pause()
          .whenComplete(() => _updateStatus(_status.toPaused()));
    }
  }

  void _updateStatus(PlayerStatus status, {bool silent = false}) {
    print("Status update: prev: $_status, new: $status");
    _status = status;
    if (statusCallback != null) {
      if (!silent) {
        statusCallback(_status);
      }
    }
  }

  @override
  void onStatusUpdated(OnStatusUpdateCallback callback) {
    statusCallback = callback;
  }

  @override
  Future seek(Duration duration) async {
    return Future.value();
  }
}

class AssetsPlayerPanel extends BasePlayerPanel<AssetsAudioPlayer> {
  AssetsPlayerPanel(BasePlayer<AssetsAudioPlayer> basePlayer)
      : super(basePlayer);

  @override
  Stream<Duration> get progress => basePlayer.player.currentPosition;

  @override
  Future seek(Duration duration) async {
    basePlayer.player.seek(duration);
  }
}
