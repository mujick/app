HeatMapComputer heatMapComputer = HeatMapComputer();
HeatMapNormalizer heatMapNormalizer = HeatMapNormalizer();

class HeatMapComputer {
  List<Cluster> compute(List<int> values, int density, int collapseValue) {
    final List<_Intensity> list = _collapse(values, collapseValue);
    List<List<_Intensity>> aggr = _aggregate(
        list, (first, second) => abs(first.value - second.value) <= density);
    return aggr.map((list) => _calculateCluster(list)).toList();
  }

  Cluster _calculateCluster(List<_Intensity> cluster) {
    int sum = 0;
    int intensity = 0;
    for (_Intensity i in cluster) {
      sum += i.value;
      intensity += i.counter;
    }
    sum = sum ~/ cluster.length;
    return Cluster(sum, cluster.length, intensity.toDouble());
  }

  List<_Intensity> _collapse(List<int> values, int collapseValue) {
    values.sort((a, b) => a.compareTo(b));
    List<_Intensity> intensities = List();
    int currentIndex = 0;
    while (currentIndex < values.length - 1) {
      int counter = 1;
      int clusterIndex = currentIndex + 1;
      while (clusterIndex < values.length &&
          abs(values[currentIndex] - values[clusterIndex]) <= collapseValue) {
        counter++;
        currentIndex++;
        clusterIndex++;
      }
      intensities.add(_Intensity(values[currentIndex], counter));
      currentIndex++;
    }
    return intensities;
  }

  List<List<T>> _aggregate<T>(List<T> list, _DiffEnough<T> diffEnough) {
    List<List<T>> aggr = List();
    int current = 0;
    while (current < list.length - 1) {
      List<T> currentList = List();
      currentList.add(list[current]);
      int clusterIndex = current + 1;
      while (clusterIndex < list.length &&
          diffEnough(list[current], list[clusterIndex])) {
        currentList.add(list[clusterIndex]);
        current++;
        clusterIndex++;
      }
      current++;
      aggr.add(currentList);
      if (current == list.length - 1) {
        aggr.add([list[current]]);
      }
    }
    return aggr;
  }

  int abs(int value) => value < 0 ? -value : value;
}

class HeatMapNormalizer {
  List<Cluster> normalize(List<Cluster> clusters, int maxPosition,
      int maxOutputPosition, int maxOutputScale, double maxIntensity) {
    double maxScaleValue = 0;
    double maxIntensityValue = 0;
    for (Cluster c in clusters) {
      if (c.scale > maxScaleValue) {
        maxScaleValue = c.scale.toDouble();
        maxIntensityValue = c.intensity;
      }
    }
    double positionFactor = maxOutputPosition / maxPosition;
    double scaleFactor = maxOutputScale / maxScaleValue;
    double intensityFactor = maxIntensity / maxIntensityValue;

    print("scaleFactor $scaleFactor");
    print("intensityFactor $intensityFactor");

    return clusters
        .map((cluster) =>
        Cluster(
            (cluster.position * positionFactor).round(),
            (cluster.scale * scaleFactor).round(),
            cluster.intensity * intensityFactor))
        .toList();
  }
}

typedef _DiffEnough<T> = bool Function(T first, T secound);

class Cluster {
  final int position;
  final int scale;
  final double intensity;

  const Cluster(this.position, this.scale, this.intensity);

  @override
  String toString() {
    return "($position, $scale, $intensity)";
  }
}

class _Intensity {
  final int value;
  final int counter;

  const _Intensity(this.value, this.counter);

  @override
  String toString() {
    return "Intensity($value, $counter)";
  }
}
