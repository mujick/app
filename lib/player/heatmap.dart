import 'package:flutter/material.dart';
import 'package:mujick/ResColors.dart';
import 'package:mujick/player/heatmapcomputer.dart';

List<int> testValues = [
  11000,
  11232,
  11234,
  11236,
  11238,
  14000,
  15300,
  30000,
  35000,
  35400,
  60000,
  61040,
  62000,
  64000,
  72000,
  72300,
  76000,
  80000,
  90000,
  92000,
  93000,
  12000
];

class TestHeatMapPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: TrackHeatMap(80, 150000, testValues)),
    );
  }
}

class TrackHeatMap extends StatelessWidget {
  final density = 11;
  final collapseFactor = 0.3;
  final double size;
  final int duration;

  final List<int> values;
  final Color color;

  const TrackHeatMap(this.size, this.duration, this.values, {this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(height: size),
      child: LayoutBuilder(
        builder: (context, constraints) {
          final double densityScale = duration / constraints.minWidth;
          final int finalDensity = (density * densityScale).toInt();
          final int collapseValue = (finalDensity * collapseFactor).round();
          print("Final density = $finalDensity");
          print("Duration = $duration");
          List<Cluster> clusters =
          heatMapComputer.compute(values, finalDensity, collapseValue);
          print(clusters);
          clusters = heatMapNormalizer.normalize(
              clusters,
              duration,
              (constraints.minWidth - size / 2).toInt(),
              constraints.minHeight ~/ 2,
              0.3);
          print(clusters);
          List<Widget> rounds = clusters
              .map((cluster) => _clusterToWidget(cluster, color))
              .toList();
          return Stack(
            children: rounds,
          );
        },
      ),
    );
  }

  Widget _clusterToWidget(Cluster cluster, Color color) {
    return _buildRound(
        cluster.scale.toDouble(), cluster.position.toDouble(), color);
  }

  Widget _buildRound(double radius, double position, Color color) {
    return Positioned(
        top: (size / 2) - radius,
        left: 0 - radius + (size / 2) + position,
        child: _RoundView(radius, color: color));
  }
}

class _RoundView extends StatelessWidget {
  final double radius;
  final Color color;

  const _RoundView(this.radius, {this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: radius * 2,
      height: radius * 2,
      decoration: BoxDecoration(
        color: this.color != null ? color : ResColors.accent.withOpacity(0.4),
        shape: BoxShape.circle,
      ),
    );
  }
}
