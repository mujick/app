import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:mujick/ResColors.dart';
import 'package:mujick/app/routing.dart';
import 'package:mujick/data/FeedbackRepository.dart';
import 'package:mujick/data/TracksRepository.dart';
import 'package:mujick/entity/Track.dart';
import 'package:mujick/player/debounce.dart';
import 'package:mujick/player/heatmap.dart';
import 'package:mujick/player/player.dart';
import 'package:mujick/player/playercontrol.dart';

final PlayerPanel player = AssetsPlayerPanel(AssetsPlayer(AssetsAudioPlayer()));
final FeedbackRepository feedbackRepository = FeedbackRepository();

class PlayerPage extends StatefulWidget {
  final Track _track;

  PlayerPage(this._track) {
    print("$PlayerPage opened with $_track");
    if (!player.status.started) {
      player.prepare(_track);
    }
  }

  @override
  _PlayerPageState createState() => _PlayerPageState();
}

class _PlayerPageState extends State<PlayerPage> {
  List<int> values;
  int _progress = 0;

  @override
  void initState() {
    super.initState();
    tracksRepository.fetchTrackHeatMapValues(widget._track.id).then((v) {
      values = v;
      print("Got heatMap for ${widget._track.id} $v");
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      margin: EdgeInsets.only(top: 25),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              child: Text(widget._track.name,
                  textAlign: TextAlign.center,
                  maxLines: 3,
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
              padding: EdgeInsets.only(left: 24, right: 24, top: 64),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: IconButton(
                padding: EdgeInsets.all(16),
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          ),
          Align(alignment: Alignment.center, child: _buildPlayerBody(context)),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 128),
              child: values == null
                  ? null
                  : Text(
                      "${values.length} reactions left on this podcast",
                      style: TextStyle(fontSize: 16),
                    ),
            ),
          )
        ],
      ),
    ));
  }

  Widget _buildPlayerBody(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        StreamBuilder<Duration>(
            stream: player.progress,
            builder: (context, snapshot) {
              int duration = snapshot.hasData && player.status.track != null
                  ? player.status.track.durationMillis
                  : 1;
              int progress =
                  snapshot.hasData ? snapshot.data.inMilliseconds : 0;
              print("Progress = $progress");
              _progress = progress;
              double height = 80;
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                        child: values == null
                            ? null
                            : TrackHeatMap(height, duration, values)),
                    Container(
                      height: height,
                      child: Center(
                        child: Slider(
                          activeColor: Colors.white,
                          min: 0,
                          max: duration.toDouble() / 1000,
                          value: progress.toDouble() / 1000,
                          onChanged: (v) {
                            player.seek(
                                Duration(milliseconds: (v * 1000).round()));
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }),
        Container(
            margin: EdgeInsets.only(top: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                _buildSwitchButton(context, false),
                PlayerControl(
                    playerPanel: player,
                    builder: (context, status) {
                      print(status);
                      return _buildPlayButton(status.isPaused);
                    }),
                _buildSwitchButton(context, true)
              ],
            )),
        Container(
          margin: EdgeInsets.only(top: 32),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _buildFeedbackButton(false),
              _buildFeedbackButton(true)
            ],
          ),
        )
      ],
    );
  }

  Widget _buildSwitchButton(BuildContext context, bool isForward) {
    return IconButton(
        icon: Icon(isForward ? Icons.last_page : Icons.first_page,
            color: Colors.white),
        iconSize: 40,
        onPressed: () async {
          Track track = tracksRepository.getTrack(widget._track, isForward);
          if (track != null) {
            player.stop();
            NavigatorState navigator = Navigator.of(context);
            navigator.pop();
            navigator.pushNamed(Router.player, arguments: track);
          }
        });
  }

  Widget _buildPlayButton(bool isPaused) {
    return Container(
        width: 80,
        height: 80,
        decoration:
            BoxDecoration(color: ResColors.accent, shape: BoxShape.circle),
        child: IconButton(
          icon: Icon(
            isPaused ? Icons.play_arrow : Icons.pause,
            color: Colors.white,
            size: 48,
          ),
          onPressed: () {
            player.playPause();
            print("Toggle play/pause");
          },
        ));
  }

  Widget _buildFeedbackButton(bool isBookmark) {
    return IconButton(
        icon: Icon(isBookmark ? Icons.bookmark : Icons.thumb_up,
            color: Colors.white),
        onPressed: () {
          if (ClickDebouncer.isClickAllowed()) {
            feedbackRepository
                .sendFeedback(widget._track.id, _progress, isBookmark, "empty")
                .then((track) {
              values = track.heatMap;
              setState(() {});
            });
            ClickDebouncer.click();
          } else {
            print("Click skipped");
          }
        },
        iconSize: 40);
  }
}
