import 'package:flutter/material.dart';
import 'package:mujick/player/player.dart';

typedef StatusWidgetBuilder = Function(BuildContext, PlayerStatus);

class PlayerControl extends StatefulWidget {
  final StatusWidgetBuilder builder;
  final PlayerPanel playerPanel;

  const PlayerControl({this.builder, @required this.playerPanel});

  @override
  _PlayerControlState createState() => _PlayerControlState();
}

class _PlayerControlState extends State<PlayerControl> {
  @override
  void initState() {
    super.initState();
    _init();
  }

  Future _init() async {
    widget.playerPanel.onStatusUpdated((status) {
      print("$PlayerControl status: $status");
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder(context, widget.playerPanel.status);
  }
}
