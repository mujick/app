import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mujick/entity/Track.dart';

final TracksRepository tracksRepository = TracksRepository();

class TracksRepository {
  final String _URL_ALL_TRACKS = "https://mujick.azurewebsites.net/music";

  List<Track> _tracks;

  Track getTrack(Track current, bool isNext) {
    if (_tracks == null) {
      throw Exception("Calling getTrack before fetching tracks");
    }
    if (current == null) {
      throw Exception("Calling getTrack on null");
    }

    int currentIndex = _tracks.indexOf(current);
    int trackIndex;
    if (isNext) {
      trackIndex = currentIndex + 1;
    } else {
      trackIndex = currentIndex - 1;
    }

    if (_tracks.length < trackIndex) {
      return null;
    } else {
      return _tracks[trackIndex];
    }
  }

  Future<List<int>> fetchTrackHeatMapValues(int id) async {
    final http.Response response = await http.get(_URL_ALL_TRACKS + "/$id");
    if (response.statusCode == 200) {
      Track track = Track.fromJson(json.decode(response.body));
      return track.heatMap;
    } else {
      throw Exception("Failed to fetch track by id: $id");
    }
  }

  Future<List<Track>> fetchTracks() async {
    final http.Response response = await http.get(_URL_ALL_TRACKS);
    if (response.statusCode == 200) {
      Iterable iterable = json.decode(response.body);
      List<Track> tracks =
          iterable.map((model) => Track.fromJson(model)).toList();
      _tracks = tracks;
      return tracks;
    } else {
      throw Exception();
    }
  }
}
