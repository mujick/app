import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mujick/entity/Track.dart';

class FeedbackRepository {
  Future<Track> sendFeedback(
      int trackId, int timestamp, bool isBookmark, String text) async {
    final String url = _buildFeedbackUrl(trackId);
    final Map<String, String> headers = Map();
    headers['Content-Type'] = 'application/json';
    final Map<String, dynamic> body = {
      'trackId': trackId,
      'timestamp': timestamp,
      'commentType': "${isBookmark ? 1 : 0}",
      'commentText': text,
      'userId': "null"
    };
    final response =
        await http.post(url, headers: headers, body: json.encode(body));
    print("$FeedbackRepository sendFeedback ${response.statusCode}");
    Track track = Track.fromJson(json.decode(response.body));
    return track;
  }

  String _buildFeedbackUrl(int trackId) {
    return "https://mujick.azurewebsites.net/music/$trackId";
  }
}
