import 'Comment.dart';

class Track {
  final int id;
  final String name;
  final String mp3Url;
  final int durationMillis;
  final List<int> heatMap;

  const Track(
      this.id, this.name, this.mp3Url, this.durationMillis, this.heatMap);

  const Track.named(
      {this.id, this.name, this.mp3Url, this.durationMillis, this.heatMap});

  factory Track.fromJson(Map<String, dynamic> json) {
    List<int> heatMap;
    try {
      Iterable iterable = json['comments'];
      heatMap = iterable.map((model) => Comment.fromJson(model)).map((c) {
        return c.timestamp;
      }).toList();
    } catch (e) {
      print(e);
      heatMap = List();
    }
    return Track.named(
        id: json['trackId'],
        name: json['name'],
        mp3Url: json['trackUrl'],
        durationMillis: 0,
        heatMap: heatMap);
  }

  @override
  String toString() {
    return "Name: $name, duration: "
        " ${Duration(milliseconds: durationMillis).inSeconds}";
  }
}
