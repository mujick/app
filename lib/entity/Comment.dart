class Comment {
  final int timestamp;
  final String commentType;

  const Comment(this.timestamp, this.commentType);

  factory Comment.fromJson(Map<String, dynamic> json) {
    return Comment(json['timestamp'], json['commentType']);
  }
}
