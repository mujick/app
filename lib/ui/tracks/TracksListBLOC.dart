import 'package:mujick/data/TracksRepository.dart';
import 'package:mujick/entity/Track.dart';

class TracksListBLOC {
  Future<List<Track>> fetchTracks() {
    return tracksRepository.fetchTracks();
  }
}

final bloc = TracksListBLOC();
