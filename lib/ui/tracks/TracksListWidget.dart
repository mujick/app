import 'package:flutter/material.dart';
import 'package:mujick/app/routing.dart';
import 'package:mujick/entity/Track.dart';
import 'package:mujick/player/playerpage.dart';
import 'package:mujick/ui/tracks/TracksListBLOC.dart';

class TracksListWidget extends State<TracksList> {
  Future<List<Track>> _tracksFuture;

  @override
  void initState() {
    super.initState();
    _tracksFuture = bloc.fetchTracks();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 25),
        child: FutureBuilder(
          future: _tracksFuture,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return _buildListView(snapshot.data);
            } else if (snapshot.hasError) {
              return Center(child: Text("error"));
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }

  Widget _buildListView(List<Track> tracks) {
    return ListView.separated(
        padding: EdgeInsets.all(16),
        itemCount: tracks.length,
        itemBuilder: (context, index) {
          return _buildListViewItem(tracks[index], index + 1);
        },
        separatorBuilder: (context, index) {
          return Divider();
        },
        physics: const AlwaysScrollableScrollPhysics());
  }

  Widget _buildListViewItem(Track track, int order) {
    return ListTile(
        leading: Text("$order"),
        title: Text(track.name),
        onTap: () async {
          Track current = player.status.track;
          print("Current $current");
          print("New $track");
          if (current?.id != track?.id) {
            await player.stop();
            print("Track $current stopped");
          }
          Navigator.of(context).pushNamed(Router.player, arguments: track);
        });
  }
}

class TracksList extends StatefulWidget {
  @override
  State createState() => TracksListWidget();
}
